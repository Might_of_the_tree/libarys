# information

this libary is building neural networks for the end user with only the necesarry information if you don't want to use stuff like activation functions then this libary is able to do it wouldn't recommend it for this task if you use a lot of the features of neural networks like genetic algorythms this libary is a good choise.

PS: this is the first release of this libary and will propably have some bucks and memory lags please report them. If you want to help at this project write me an e-mail.

# hebbian

## definitions
to chose a Weight for every connection use
> #define standardWeight

the standart is 1

## constructors
the variables used are declared [here](#hebbian##variables)
1. > hebbianNN(std::vector<int> &ViNeuronC, std::vector<double*> &Vinput, std::vector<double> &VftdBias, std::vector<std::vector<double& (*)(double&)>> &aktivation, std::vector<int*> &connections);

this constructor creates bias, input, hidden and output neurons the activation funktion is x=y and the connections from a neuron are always headed to all neurons in the next layer.

## functions
1. > std::vector<double>& run();

the return value are the outputs of the neural network. this functions runs the neural network.

2. > void learn();

this functions changes the weights of the neural network

## variables
1. ViNeuronC

this variable is the information how many hidden and output layers you want to create and how many neurons are in the layers

| neuron count hidden layer 1 | ... | neuron counthidden layer x-1     | output layer                     |
|-----------------------------|-----|----------------------------------|----------------------------------|
| ViNeuronC[0]                |     | ViNeuronC[ViNeuronC.getsize()-2] | ViNeuronC[ViNeuronC.getsize()-1] |

2. Vinput

this variable stores the pointers to the inputs of the neural network

3. VftdBias

this variable stores the diffrent bias values

4. akt

this variable stores the aktivation function for each neuron if the size is 0  every function will use y=x

5. connections

this variable stores all the connections if the size is 0 every neuron will have 1 connection to the every neuron of the next layer
