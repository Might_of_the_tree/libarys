# !!!this needs an update it currently dosen't run because the vector Libary is changed


# bit simulation
bit simulation is a Libary to simulate numbers with a maximum of 16GB it dosn't use the memory perfect memory los is Size*int64_MAX/2+Size
this Libary is new please report errors

# current version: 0.0.1

##  _RESIZEBLE_int

[constructors](./bit_simulation.md###constructor)

[operators](./bit_simulation.md###operators)

[functions](./bit_simulation.md###funciotns)

### constructors

1. standart constructor
>  _RESIZEBLE_int();

2. recommended constructor
>  _RESIZEBLE_int(const int &iStartMemory_x8);

*iStartMemory_x8* is the Size of the created objekt if it is 1 64 bit get allocated if it is 2 128 bit get allocated...

3. load from drive constructor
> _RESIZEBLE_int(const std::string &strPath);

*strPath* is the path to the file in which the number is saved
it will throw -1 if the file cannot be opened

### operators
the operators work like the standart operators they all need another BIC::_RESIZEBLE_int to work besides the operator ++ and --
> bool operator< (BIC::_RESIZEBLE_int &other);

> bool operator> (BIC::_RESIZEBLE_int &other);

> bool operator<=(BIC::_RESIZEBLE_int &other);

> bool operator>=(BIC::_RESIZEBLE_int &other);

> bool operator==(BIC::_RESIZEBLE_int &other);

> bool operator!=(BIC::_RESIZEBLE_int &other);

> BIC::_RESIZEBLE_int& operator+(BIC::_RESIZEBLE_int &other);

> BIC::_RESIZEBLE_int& operator*(BIC::_RESIZEBLE_int &other);

> BIC::_RESIZEBLE_int& operator-(BIC::_RESIZEBLE_int &other);

> BIC::_RESIZEBLE_int& operator/(BIC::_RESIZEBLE_int &other);

> void operator=(BIC::_RESIZEBLE_int &other);

> void operator+=(BIC::_RESIZEBLE_int &other);

> void operator-=(BIC::_RESIZEBLE_int &other);

> void operator*=(BIC::_RESIZEBLE_int &other);

> void operator/=(BIC::_RESIZEBLE_int &other);

> void operator++();

> void operator--();

### functions

1. this function switches to numbers
> void _switch(BIC::_RESIZEBLE_int &other);

2. this function works like a move constructor
> void move(BIC::_RESIZEBLE_int &other);

3. this function writes a number to your drive
> void WriteToDrive(const std::string &strPath);

*strPath* is the path to file in which the number will be saved
it will throw -1 if the file cannot be opened

4. this function converts this *_RESIZEBLE_int* to a *_STATIC_int*
> BIC::_STATIC_int& convertTo_STATIC();

_STATIC_int comes in the new version


# minnor
I want to add a function to deallocate useless memory

# new version
I want to add a class which is a number with a static size