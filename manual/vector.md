# information
*current version 2.1.2*

This library is very fast but unsecured if you use it check if the memory you call is allocated and make sure you create your buffer your self functions for all classes *D_vector* is for variables and *C_vector* for classes structs usw...

PS: if you wan't to add a test write it in the test file if you find an bug write me an e-mail

# general functions

[constructor](#constructor)

[operator++](#operator++)

[operator--](#operator--)

[size_t get_size()](#get_size)

[T& get_value()](#get_value)

[T* get_ValPtr()](#get_ValPtr)

[set_iterator()](#set_iterator)

[push_back(const size_t &index)](#push_back)

[push_front(const size_t &index)](#push_front)

[deleteback(const size_t &index)](#deleteback)

[deletefront(const size_t &index)](#deletefront)

# D_vector specific functions

[operator+](#operator+)

[operator-](#operator-)

[operator+=](#operator+=)

[operator-=](#operator-=)

# constructor
## standard constructor
the standard constructor does set the size to 0 but does nothing else
## constructor(const size_t &size)
this constructor allocates an array of the length size, sets the value pointer to the begin of the array and sets the size to size
## advise
don't use the standard constructor if possible it is a bit slower

# operator++
increase the iterator by 1

#operator--
lower the iterator by 1

# get_size
get_size returns a copy of m_size

# get_value
get_value returns a reference to the content of the iterator

# get_ValPtr
get_ValPtr returns the value pointer

# set_iterator
set_iterator requires a const size_t reference to index and sets the iterator to m_ValPtr+index

# operator=
the operator = copy's all data from one two the other vector if you want to copy only one variable or class use the operator [] and the the operator = from the c++ standard

> object[x]=other_object[y];

of course you can do this with two variables from the same vector

# push_back
push_back creates a new vector with the size m_size+index and copy's all data to the start of the new vector in the end you'll have more memory but it takes really long it is faster than push front

# push_front
push_front creates a new vector with the size m_size+index and copy's all data to the end of the new vector in the end you'll have more memory but it takes really long

# deleteback
deleteback deletes index from the end of the vector

# deletefront
deletefront deletes index from the start of the vector it is faster than deleteback

# operator+
the operator + adds two vectors and returns the result as a third one
ATTENTION! just for D_vector

# operator-
the operator - sub one vector from the other and returns the result as a third one
ATTENTION! just for D_vector

# operator+=
> this_object=this_object+other_object

ATTENTION! just for D_vector

# operator-=
> this_object=this_object-other_object

ATTENTION! just for D_vector
