# Libraries

this project contains my self written libraries and their manuals

if you want to check waht will be added to the libraries or what will be changed take a look at the [status file](./status.md)

## [vector](./manual/vector_manual.md)
vector is a library which creates arrays and manages them. It is optimised for datamining in neural networks
note: this library has no own memory allocator I will add the STL memory allocator or use my own memory allocators

## [bit_simulation](./manual/bit_simulation.md)
bit_simulation is a library to calculate big numbers with up to 32GB per number
Note: this project is stil in progress get information in the [status file](./status.md)

## [neural network](./manual/neural_network.md)
this is a library to create and learn with diffrent learning methods in neural networks
Note: this project is stil in progress get information in the TODOs are in the [cpp file](./NeuralNetwork/hebbian.cpp) of the hebbian learning algorythm