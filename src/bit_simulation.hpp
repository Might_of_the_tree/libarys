/*                                                  
          `-`                        .-           
          +s/                       `os:          
         :sss.                      /sso`         
        `ossso`                    .ssss+         
        +sssss/                   `osssss:        
       :sssssss.                  /sssssso`       
      `ossssssso                 .ssssssss+       
      +sssssssss/               `osssssssss:      
     ./++++++++++ossssssssssssss+++++++++++/`     
    `:///++::::++:oo/sss/ossssso+++//++++///:     
    :////-`:////+-++ //s`+ssso/--/+-.:-://///.    
   ./////`-/:--/+ /+ oos`+ssso//- +-./:`.////:`   
   `-:///..:/+.:+ // sss`+sss-./: +-.//..///:.    
      .-::-....:+./+--/s-:::/:.--.+-....::-`      
         .://////++++osssssso++++/////:-`         
           `.:////++++osssso+++////:-.            
              `-:///++ossso+++///:.               
                 `-:/++osso+//:.`                 
                    .-/+oo+/-`                    
                       ./:'
*/                            
                                                  


//version 0.0.0

#include <cstdint>
#include <string>
#include "vector.hpp"



namespace BIC{

    struct _STATIC_int{

	    D_vector<int64_t> Vi64Memory;//memory for the number

        _STATIC_int();//allocates 128 Bytes
        _STATIC_int(const int &iStartMemory_x8);//allocates iStartMemory_x8 * 8 Bytes
        _STATIC_int(const std::string &strPath);//Loads a number from the Drive
        _STATIC_int(BIC::_STATIC_int && other);//move constructor

        bool operator< (BIC::_STATIC_int &other);
        bool operator> (BIC::_STATIC_int &other);
        bool operator<=(BIC::_STATIC_int &other);
        bool operator>=(BIC::_STATIC_int &other);
        bool operator==(BIC::_STATIC_int &other);
        bool operator!=(BIC::_STATIC_int &other);
        BIC::_STATIC_int& operator+(BIC::_STATIC_int &other);
        BIC::_STATIC_int& operator*(BIC::_STATIC_int &other);
        BIC::_STATIC_int& operator-(BIC::_STATIC_int &other);
        BIC::_STATIC_int& operator/(BIC::_STATIC_int &other);
        void operator=(BIC::_STATIC_int &other);
        void operator+=(BIC::_STATIC_int &other);
        void operator-=(BIC::_STATIC_int &other);
        void operator*=(BIC::_STATIC_int &other);
        void operator/=(BIC::_STATIC_int &other);
        void operator++(int);
        void operator--(int);

        void _switch(BIC::_STATIC_int &other);//switch this with the other _STATIC_int
        void move(BIC::_STATIC_int &other);//moves the other number to this class the content will be deleted
        void WriteToDrive(const std::string &strPath);//writes this class to the Drive

    };
    //intager wich resizes it self to be able to save every value
    struct _RESIZEBLE_int{

        D_vector<int64_t> Vi64Memory;//memory for the number

        _RESIZEBLE_int(const int &iStartMemory_x8);//allocates memory 8 times bigger than iStartMemory_x8
        _RESIZEBLE_int();//allocates 128 Bytes
        _RESIZEBLE_int(const std::string &strPath);//Loads a number from the Drive
        _RESIZEBLE_int(BIC::_RESIZEBLE_int && other);//move constructor

        bool operator< (BIC::_RESIZEBLE_int &other);
        bool operator> (BIC::_RESIZEBLE_int &other);
        bool operator<=(BIC::_RESIZEBLE_int &other);
        bool operator>=(BIC::_RESIZEBLE_int &other);
        bool operator==(BIC::_RESIZEBLE_int &other);
        bool operator!=(BIC::_RESIZEBLE_int &other);
        BIC::_RESIZEBLE_int& operator+(BIC::_RESIZEBLE_int &other);
        BIC::_RESIZEBLE_int& operator*(BIC::_RESIZEBLE_int &other);
        BIC::_RESIZEBLE_int& operator-(BIC::_RESIZEBLE_int &other);
        BIC::_RESIZEBLE_int& operator/(BIC::_RESIZEBLE_int &other);
        void operator=(BIC::_RESIZEBLE_int &other);
        void operator+=(BIC::_RESIZEBLE_int &other);
        void operator-=(BIC::_RESIZEBLE_int &other);
        void operator*=(BIC::_RESIZEBLE_int &other);
        void operator/=(BIC::_RESIZEBLE_int &other);
        void operator++(int);
        void operator--(int);
        
        void _switch(BIC::_RESIZEBLE_int &other);//switch this with the other _RESIYEBLE_int
        void move(BIC::_RESIZEBLE_int &other);//moves the other number to this class the content will be deleted
        void delete_trash();
        void WriteToDrive(const std::string &strPath);//writes this class to the Drive
        BIC::_STATIC_int& convertTo_STATIC();//convert this class to a BIC::_STATIC_int

    };

}

#include "bit_simulation.cpp"
