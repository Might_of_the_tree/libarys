/*
          `-`                        .-
          +s/                       `os:
         :sss.                      /sso`
        `ossso`                    .ssss+
        +sssss/                   `osssss:
       :sssssss.                  /sssssso`
      `ossssssso                 .ssssssss+
      +sssssssss/               `osssssssss:
     ./++++++++++ossssssssssssss+++++++++++/`
    `:///++::::++:oo/sss/ossssso+++//++++///:
    :////-`:////+-++ //s`+ssso/--/+-.:-://///.
   ./////`-/:--/+ /+ oos`+ssso//- +-./:`.////:`
   `-:///..:/+.:+ // sss`+sss-./: +-.//..///:.
      .-::-....:+./+--/s-:::/:.--.+-....::-`
         .://////++++osssssso++++/////:-`
           `.:////++++osssso+++////:-.
              `-:///++ossso+++///:.
                 `-:/++osso+//:.`
                    .-/+oo+/-`
                       ./:'
*/


#pragma once



typedef long unsigned int size_t;


//Container with Data types
template<typename T>
class D_vector{
private:
	T* m_ValPtr;
	T* m_iterator;
	size_t m_size;

public:
	D_vector();
	D_vector(const size_t &size);
	~D_vector();
	D_vector(     D_vector<T> &&other);
	D_vector(const D_vector<T> &other);

	T& operator[](const size_t &index);
	D_vector<T>& operator+(D_vector<T> &other);
	D_vector<T>& operator-(D_vector<T> &other);
	void operator++(int);
	void operator--(int);
	void operator-=(const D_vector<T> &other);
	void operator+=(const D_vector<T> &other);
	void operator=(const D_vector<T>  &other);
	void operator=(	     D_vector<T> &&other);

	size_t    get_size();
	T& get_vectorValue();
	T&       get_Value();
	void set_iterator(const size_t &index);
	T* get_ValPtr();
	void push_back  (const size_t &index);
	void deleteback (const size_t &index);
	void push_front (const size_t &index);
	void deletefront(const size_t &index);
	void delete_    (const size_t &valNum);

};


//Container with classes
template<typename T>
class C_vector{
private:
	T* m_ValPtr;
	T* m_iterator;
	size_t m_size;

public:
	C_vector();
	C_vector(const size_t &size);
	~C_vector();
	C_vector(     C_vector<T> &&other);
	C_vector(const C_vector<T> &other);

	T&   operator[](    const size_t &index);
	void operator=(const C_vector<T> &other);//uses copy constructor of T
	void operator=(	    C_vector<T> &&other);
	void operator++(int);
	void operator--(int);

	T& get_Value();
	size_t get_size();
	void  set_iterator(const size_t &index);
	T* get_ValPtr();

	void push_back(const size_t &index);
	void deleteback(const size_t &index);
	void push_front(const size_t &index);
	void deletefront(const size_t &index);
	void delete_(const size_t &place);

};
