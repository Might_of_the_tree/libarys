#include <cstring>
#include "vector.hpp"


template<typename T>
D_vector<T>::D_vector(){

	m_size=0;
	m_ValPtr=nullptr;
	m_iterator=nullptr;

}

template<typename T>
D_vector<T>::D_vector(const size_t &size)
: m_size(size){

	//Create an array to save the data
	m_ValPtr = new T[size];
	m_iterator = m_ValPtr;

}

template<typename T>
D_vector<T>::D_vector(D_vector<T> &&other){

	m_ValPtr   = other.m_ValPtr  ;
	m_size     = other.m_size    ;
	m_iterator = other.m_iterator;
	other.m_size =          0    ;

}

template<typename T>
D_vector<T>::~D_vector(){

	for (size_t i = 0; i < m_size; i++)
		delete (m_ValPtr+i);

}

template<typename T>
T& D_vector<T>::operator[](const size_t &index){

	return m_ValPtr[index];

}

template<typename T>
void D_vector<T>::operator=(const D_vector<T> &other){

	if(other.m_size < m_size){

		this->deletefront((m_size-other.m_size));

		memcpy(m_ValPtr, other.m_ValPtr, m_size*sizeof(T));

		return;

	}else{

		D_vector<T> help(other.m_size);

		for(size_t i=0; i < m_size; i++)//delete the old array
			delete (m_ValPtr+i);

		memcpy(help.m_ValPtr, other.m_ValPtr, help.m_size*sizeof(T));

		m_ValPtr=help.m_ValPtr;
		m_size=help.m_size;
		m_iterator=m_ValPtr;

		help.m_size=0;

		return;

	}

}

template<typename T>
void D_vector<T>::operator=(D_vector<T> &&other){

	m_ValPtr = other.m_ValPtr  ;
	m_size   = other.m_size    ;
	m_iterator=other.m_iterator;
	other.m_size =            0;

}

template<typename T>
D_vector<T>& D_vector<T>::operator+(D_vector<T> &other){

	if(m_size < other.m_size){

		static D_vector<T> retVal(other.m_size);

		for(size_t i=0; i < m_size; i++)
			retVal[i]=m_ValPtr[i]+other[i];

		for(size_t i=m_size; i < other.m_size; i++)
			retVal[i]=other[i];

		return retVal;

	}else{

		static D_vector<T> retVal(m_size);

		for(size_t i=0; i < other.m_size; i++)
			retVal[i]=m_ValPtr[i]+other[i];

		for(size_t i=other.m_size; i < m_size; i++)
			retVal[i]=m_ValPtr[i];

		return retVal;

	}

}

template<typename T>
D_vector<T>& D_vector<T>::operator-(D_vector<T> &other){

	if(m_size < other.m_size){

		static D_vector<T> retVal(other.m_size);

		for(size_t i=0; i < m_size; i++)
			retVal[i]=m_ValPtr[i]-other[i];

		for(size_t i=m_size; i < other.m_size; i++)
			retVal[i]=0-other[i];

		return retVal;

	}else{

		static D_vector<T> retVal(m_size);

		for(size_t i=0; i < other.m_size; i++)
			retVal[i]=m_ValPtr[i]-other[i];

		for(size_t i=other.m_size; i < m_size; i++)
			retVal[i]=m_ValPtr[i];

		return retVal;

	}

}

template<typename T>
void D_vector<T>::operator++(int){

	m_iterator++;

}

template<typename T>
inline void D_vector<T>::operator--(int){

	m_iterator--;

}

template<typename T>
T* D_vector<T>::get_ValPtr(){

	return m_ValPtr;

}

template<typename T>
void D_vector<T>::operator-=(const D_vector<T> &other){

	(*this)=(*this)-other;

}

template<typename T>
void D_vector<T>::operator+=(const D_vector<T> &other){

	(*this)=(*this)+other;

}

template<typename T>
size_t D_vector<T>::get_size(){

	return m_size;

}

template<typename T>
T& D_vector<T>::get_vectorValue(){

	static T retVal;

	for (size_t i = 0; i < m_size; i++)
		retVal+=m_ValPtr[i];

	return retVal;

}

template<typename T>
T& D_vector<T>::get_Value(){

	return *m_iterator;

}

template<typename T>
void  D_vector<T>::set_iterator(const size_t &index) {

	m_iterator=m_ValPtr+index;

}

template<typename T>
void D_vector<T>::push_back(const size_t &index){

	T* hpValPtr;

	//creates a new and bigger array
	hpValPtr = new T[m_size+index];

	memcpy(hpValPtr, m_ValPtr, (m_size*sizeof(T)));

	for(size_t i=0; i < m_size; i++)//delete the old array
		delete (m_ValPtr+i);

	//change Size
	m_size=+index;
	//makes the new arrray to the default array
	m_ValPtr = hpValPtr;
	m_iterator = m_ValPtr;

}

template<typename T>
void D_vector<T>::deleteback(const size_t &index){

	//delete and start at the end
	for(size_t i=0; i < index; i++)
		delete (m_ValPtr+(m_size-i));

	m_size=-index;

}

template<typename T>
void D_vector<T>::push_front(const size_t &index){

	T* hpValPtr;

	//creates a new and bigger array
	hpValPtr = new T[(m_size+index)];

	memcpy(hpValPtr+index, m_ValPtr, (m_size*sizeof(T)));

	for(size_t i=0; i < m_size; i++)//delete the old array
		delete (m_ValPtr+i);

	//change Size
	m_size=+index;
	//makes the new arrray to the default array
	m_ValPtr = hpValPtr;
	m_iterator = m_ValPtr;

}

template<typename T>
void D_vector<T>::deletefront(const size_t &index){

	//delete a chosed number of datatypes
	for(size_t i=0; i < index; i++)
		delete (m_ValPtr+i);

	m_ValPtr=m_ValPtr+index;
	m_size=-index;

}

template<typename T>
void D_vector<T>::delete_(const size_t &valNum){

	for(size_t i=valNum; i < m_size; i++)
		m_ValPtr[i]= m_ValPtr[i+1];

	delete (m_ValPtr+m_size);
	m_size--;

}





//all is the same as the first class but it is working with classes and not with datatypes
template<typename T>
C_vector<T>::C_vector(){

	m_size =  0;
	m_ValPtr = nullptr;
	m_iterator = nullptr;

}

template<typename T>
C_vector<T>::C_vector(const size_t &size)
: m_size(size){

	m_ValPtr = new T[m_size];
	m_iterator = m_ValPtr;

}

template<typename T>
C_vector<T>::~C_vector(){

	for(size_t i = 0; i < m_size; i++)
		delete (m_ValPtr+i);

}

template<typename T>
C_vector<T>::C_vector(C_vector<T> &&other){

	for(size_t i = 0; i < m_size; i++)
		delete (m_ValPtr+i);

	m_ValPtr = other.m_ValPtr		 ;
	m_size   = other.m_size  		 ;
	m_iterator = other.m_iterator;

	other.m_size=0;

}

template<typename T>
C_vector<T>::C_vector(const C_vector<T> &other){

	if(other.m_size < m_size){

		this->deletefront((m_size-other.m_size));

		for(size_t i=0; i < m_size; i++)
			m_ValPtr[i] = other[i];

	}else{

		C_vector<T> help(other.m_size);

		for(size_t i=0; i < m_size; i++)//delete the old array
			delete (m_ValPtr+i);

		memcpy(help.m_ValPtr, other.m_ValPtr, (help.m_size*sizeof(T)));

		m_ValPtr=help.m_ValPtr;
		m_size=help.m_size;
		m_iterator = m_ValPtr;

		help.m_size=0;

	}

}

template<typename T>
T& C_vector<T>::operator[](const size_t &index){

	return m_ValPtr[index];

}

template<typename T>
void  C_vector<T>::operator=(C_vector<T> &&other) {

	for(size_t i = 0; i < m_size; i++)
		delete (m_ValPtr+i);

	m_ValPtr = other.m_ValPtr		 ;
	m_size   = other.m_size  		 ;
	m_iterator = other.m_iterator;

	other.m_size=0;

}

template<typename T>
void C_vector<T>::operator=(const C_vector<T> &other){

	if(other.m_size < m_size){

		this->deletefront((m_size-other.m_size));

		for(size_t i=0; i < m_size; i++)
			m_ValPtr[i] = other[i];

	}else{

		C_vector<T> help(other.m_size);

		for(size_t i=0; i < m_size; i++)//delete the old array
			delete (m_ValPtr+i);

		memcpy(help.m_ValPtr, other.m_ValPtr, (help.m_size*sizeof(T)));

		m_ValPtr=help.m_ValPtr;
		m_size=help.m_size;
		m_iterator = m_ValPtr;

		help.m_size=0;

	}

}

template<typename T>
void C_vector<T>::operator++(int) {

	m_iterator++;

}

template<typename T>
void C_vector<T>::operator--(int) {

	m_iterator--;

}

template<typename T>
T* C_vector<T>::get_ValPtr() {

	return m_ValPtr;

}

template<typename T>
size_t C_vector<T>::get_size() {

	return m_size;

}

template<typename T>
T& C_vector<T>::get_Value() {

	return *m_iterator;

}

template<typename T>
void C_vector<T>::set_iterator(const size_t &index) {

	m_iterator = m_ValPtr+index;

}

template<typename T>
void C_vector<T>::push_back(const size_t &index){

	T* hpValPtr;

	//creates a new and bigger array
	hpValPtr = new T[(m_size+index)];

	memcpy(hpValPtr, m_ValPtr, (m_size*sizeof(T)));

	for(size_t i=0; i < m_size; i++)//delete the old array
		delete (m_ValPtr+i);

	//change Size
	m_size=+index;
	//makes the new arrray to the default array
	m_ValPtr = hpValPtr;
	m_iterator = m_ValPtr;
	//set the old pointer to a null pointer
	hpValPtr=nullptr;

}

template<typename T>
void C_vector<T>::deleteback(const size_t &index){

	//delete and start at the end
	for(size_t i=0; i < index; i++)
		delete (m_ValPtr+(m_size-i));

	m_size=-index;
}

template<typename T>
void C_vector<T>::push_front(const size_t &index){

	T* hpValPtr;

	//creates a new and bigger array
	hpValPtr = new T[(m_size+index)];

	memcpy(hpValPtr+index, m_ValPtr, (m_size*sizeof(T)));

	for(size_t i=0; i < m_size; i++)//delete the old array
		delete (m_ValPtr+i);

	//change Size
	m_size=+index;
	//makes the new arrray to the default array
	m_ValPtr = hpValPtr;
	m_iterator = m_ValPtr;
	//set the old pointer to a null pointer
	hpValPtr=nullptr;

}

template<typename T>
void C_vector<T>::deletefront(const size_t &index){

	//delete a chosed number of datatypes
	for(size_t i=0; i < index; i++)
		delete (m_ValPtr+i);

	m_ValPtr=m_ValPtr+index;
	m_size=-index;

}

template<class T>
void C_vector<T>::delete_(const size_t &place){

	for(size_t i=place; i < m_size; i++)
		m_ValPtr[i] = m_ValPtr[i+1];

	delete (m_ValPtr+m_size);
	m_size--;

}
