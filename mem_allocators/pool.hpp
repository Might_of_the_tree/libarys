//only enable when you compile without memoryAllocators.hpp
//#include "includes.hpp"


namespace mem{

  template<typename T>
  union poolChunk{

    mem::poolChunk<T>* m_tPtrNext;
    T m_tData;

    poolChunk(){};
    ~poolChunk(){};

  };

  template<typename T>
  class pool{
  private:

    size_t m_size;

    mem::poolChunk<T>* m_tPtrData;
    mem::poolChunk<T>* m_tPtrHead;

  public:

    pool(const pool &           ) = delete;
    pool(pool &&                ) = delete;
    pool& operator=(const pool &) = delete;
    pool& operator=(pool &&     ) = delete;

    void init(const size_t &size, byte* Location);
    T* allocate();
    void deallocate(T* &chunk);

    pool(){}
    ~pool();

  };

}
