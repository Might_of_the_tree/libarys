template<typename T>
void mem::pool<T>::init(const size_t &size, byte* Location) {

  if(nextMultiple<size_t>(alignof(T), (size_t)Location) != 0)
    throw "pool-init: alignof T dosen't fit to Location";

  if(Location == nullptr){

    m_tPtrHead = new poolChunk<T>[m_size];
    m_tPtrData = m_tPtrHead;

  }else{
    
    m_tPtrHead = (T*)Location;
    m_tPtrData = m_tPtrHead;

  }
  
  m_size = size-1;

  for (size_t i = 0; i < m_size; i++)
    m_tPtrHead[i].m_tPtrNext = m_tPtrHead+i+1;

  m_size++;

  m_tPtrHead[m_size].m_tPtrNext = nullptr;

}

template<typename T>
T* mem::pool<T>::allocate(){

  poolChunk<T>* tempPtrData = m_tPtrHead;
  m_tPtrHead = m_tPtrHead->m_tPtrNext;

  return &tempPtrData->m_tData;

}

template<typename T>
void mem::pool<T>::deallocate(T* &data) {

  data->~T();

  poolChunk<T>* tempPtrData = reinterpret_cast<poolChunk<T>*>(data);

  if(tempPtrData < m_tPtrData || tempPtrData > m_tPtrData + m_size)
    throw data;

  m_tPtrHead->m_tPtrNext = tempPtrData;

}

template<typename T>
mem::pool<T>::~pool(){

  delete[] m_tPtrData;

  m_tPtrData = nullptr;
  m_tPtrHead = nullptr;

}
