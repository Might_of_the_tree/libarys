namespace mem{

  template<typename T>
  T constexpr nextMultiple(T multipleOf, T value){

    T multiple = multipleOf + value -1;
    multiple -= (multiple % multipleOf);
    return multiple;

  }

}
