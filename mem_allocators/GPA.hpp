//only enable when you compile without memoryAllocators.hpp
//#include "includes.hpp"

namespace mem {

    class GPA{
    public:

        class GPAchunk{
        private:
            size_t m_chunkSize;
            byte*  m_bPtrData ;

            void get_memory(const size_t &amount);
            void moveAndResize(byte* newaddr, const size_t &newSize);

            GPAchunk(byte* bPtrData, size_t chunkSize)
            : m_bPtrData(bPtrData), m_chunkSize(chunkSize) {}

        public:

            bool operator<(const GPAchunk &other) const { return m_bPtrData < other.m_bPtrData; }
            bool operator<(byte* other          ) const { return m_bPtrData < other;            }
            bool operator>(const GPAchunk &other) const { return m_bPtrData > other.m_bPtrData; }
            bool operator>(byte* other          ) const { return m_bPtrData > other;            }
            const mem::GPA::GPAchunk* operator&() const { return &(*this); }

            friend mem::GPA;

        };

        template<typename T>
        class GPApointer{
        private:
            size_t m_size;
            T* m_tPtrData;
            T* m_iterator;

            GPApointer(const size_t &size, T* tPtrData)//should be private
            : m_size(size), m_tPtrData(tPtrData){}

            template<typename T2>
            inline typename std::enable_if<std::is_trivially_move_constructible<T2>::value>::type
                move(void* newaddr);

            template<typename T2>
            inline typename std::enable_if<!std::is_trivially_move_constructible<T2>::value>::type
                move(void* newaddr);

            template<typename T2>
            inline typename std::enable_if<std::is_trivially_destructible<T2>::value>::type
                destructor(void* destructAddr){
                    //do nothing
                }

            template<typename T2>
            inline typename std::enable_if<!std::is_trivially_destructible<T2>::value>::type
                destructor(void* destructAddr){
                    std::static_pointer_cast<T>(destructAddr)->~T();
                }

        public:
            T& operator->() { return *m_tPtrData; }
            const T& operator->() const { return *m_tPtrData; }
            T& operator[](const size_t &index) { return m_tPtrData[index]; }
            const T& operator[](const size_t &index) const { return m_tPtrData[index]; }
            T& operator *(){ return *m_tPtrData; }
            const T& operator*() const { return *m_tPtrData; }
            T& operator+(const size_t &amount) { return m_tPtrData + amount; }
            const T& operator+(const size_t &amount) const { return m_tPtrData; }
            T& operator-(const size_t &amount){ return m_tPtrData - amount; }
            const T& operator-(const size_t &amount) const { return m_tPtrData -amount; }
            void operator++(int) { m_iterator++; }
            void operator--(int) { m_iterator--; }
            bool operator< (const T* &other) const { return m_iterator <  other; }
            bool operator> (const T* &other) const { return m_iterator >  other; }
            bool operator>=(const T* &other) const { return m_iterator >= other; }
            bool operator<=(const T* &other) const { return m_iterator <= other; }
            bool operator==(const T* &other) const { return m_iterator == other; }
            bool operator!=(const T* &other) const { return m_iterator != other; }


            T* begin() { return m_tPtrData; }
            T* end  () { return m_tPtrData + m_size; }
            T& getIteratorData() { return *m_iterator; }
            const T& getIteratorData() const { return *m_iterator; }
            void setIterator(T* newIterator) { m_iterator = newIterator; }
            T* getRaw(){ return m_tPtrData; }
            inline size_t align(){ return alignof(T); }

            friend mem::GPA;

        };

        class HandleTable {
        private:
            void* m_DataPtrOfGPAPtr;
            void(*movePtr)(void*);
            void(*destructor)(void*);
            size_t(*align)();

            void move(void* newaddr);

            HandleTable(void* DataPtr, void (*movePtr)(void*), void (*destructor)(void*), size_t(*align)())
            : m_DataPtrOfGPAPtr(DataPtr), movePtr(movePtr), destructor(destructor), align(align){}

        public:

            bool operator< (const HandleTable &other) const { return (m_DataPtrOfGPAPtr) <  (other.m_DataPtrOfGPAPtr); }
            bool operator> (const HandleTable &other) const { return (m_DataPtrOfGPAPtr) >  (other.m_DataPtrOfGPAPtr); }
            bool operator<=(const HandleTable &other) const { return (m_DataPtrOfGPAPtr) <= (other.m_DataPtrOfGPAPtr); }
            bool operator>=(const HandleTable &other) const { return (m_DataPtrOfGPAPtr) >= (other.m_DataPtrOfGPAPtr); }
            bool operator==(const HandleTable &other) const { return (m_DataPtrOfGPAPtr) == (other.m_DataPtrOfGPAPtr); }
            bool operator!=(const HandleTable &other) const { return (m_DataPtrOfGPAPtr) != (other.m_DataPtrOfGPAPtr); }
            const mem::GPA::HandleTable* operator&()  const { return &(*this); }

            friend mem::GPA;
        
        };

    private:

        size_t m_size;
        byte* m_bPtrData;

        std::set<mem::GPA::GPAchunk, std::less<mem::GPA::GPAchunk>> m_freeChunks;
        std::set<mem::GPA::HandleTable, std::less<mem::GPA::HandleTable>> m_allocatedChunks;

        template<typename T, typename... arguments>
        inline typename std::enable_if<std::is_trivially_constructible<T>::value>::type
            constructor(T* object, const size_t &amount, arguments&&... args){
                //do nothing
            }

        template<typename T, typename... arguments>
        inline typename std::enable_if<!std::is_trivially_constructible<T>::value>::type
            constructor(T* object, const size_t &amount, arguments&&... args);
        
    public:
        
        template<typename T, typename... arguments>
        bool getSpace(T* begin, const size_t &ammount, arguments&&... args);
        void defragment();
        void init(const size_t &size, byte* Location);
        template<typename T, typename... arguments>
        GPApointer<T> allocate(const size_t &size, arguments&&... args);
        template<typename T>
        void deallocate(GPApointer<T> pointer);
        void clear();

    };
    
} // namespace mem
