template<typename T>
mem::stackDestructor::stackDestructor(const T &data)
: m_vPtrData(std::addressof(data)){
  destructor = [](const void* lambdaData){
    auto originalType = static_cast<T>(lambdaData);
    originalType->~T();
  };
}

void mem::stackDestructor::operator()(){

  destructor(m_vPtrData);

}

void mem::stack::init(const size_t &size, byte* Location) {

  if(Location == nullptr){

    m_bPtrData = new byte[size];
    m_bPtrHead = m_bPtrData;
    m_size = size;

  }else{
  
    m_bPtrData = Location;
    m_bPtrHead = m_bPtrData;
    m_size = size;

  }
  

}

void* mem::stack::allocate(const size_t &bytesC, const size_t &allignment){

  byte* retVal = (byte*)mem::nextMultiple(allignment, (size_t)m_bPtrHead);
  byte* newHeadPtr = retVal + bytesC;

  if(newHeadPtr > m_bPtrData + m_size){

    return nullptr;

  }

  m_bPtrHead = newHeadPtr;
  return retVal;

}

template <typename T, typename... arguments>
T* mem::stack::allocateObject(size_t ObjectsC, arguments&&... args){

  byte* allocationLocation = (byte*)nextMultiple(alignof(T), (size_t)m_bPtrHead);
  byte* newHeadPtr = allocationLocation + ObjectsC * sizeof(T);
  T* retVal = reinterpret_cast<T*>(allocationLocation);

  if(newHeadPtr > m_bPtrData + m_size){

    return nullptr;

  }

  for (size_t i = 0; i < ObjectsC; i++) {
    T* objects = new(retVal+i) T(std::forward<arguments>(args)...);
    addDestructorToList(objects);
  }

  m_bPtrHead = newHeadPtr;
  return retVal;

}

template<typename T, typename... arguments>
bool mem::stack::getSpace(T* begin, const size_t &ammount, arguments&&... args){

  if(m_bPtrHead == (byte*)begin && (byte*)begin+(ammount * sizeof(T)) <= m_bPtrData + m_size){

    m_bPtrHead = m_bPtrHead + ammount * sizeof(T);

    for (size_t i = 0; i < ammount; i++)
    {
      addDestructorToList<T>(std::addressof(begin[i]));
      new(std::addressof(begin[i])) T(std::forward<arguments>(args)...);
    }
    

  }

  return false;

}

void mem::stack::deallocateALL() {

  m_bPtrHead = m_bPtrData;

  while (m_vDestructors.size() > 0) {
    m_vDestructors.back()();
    m_vDestructors.pop_back();
  }

}

void mem::stack::deallocate(const stackMarker &marker) {

  m_bPtrHead = marker.m_bPtrData;

  while (m_vDestructors.size() > marker.m_destructorC) {
    m_vDestructors.back()();
    m_vDestructors.pop_back();
  }

}

mem::stackMarker mem::stack::get_marker(){
  mem::stackMarker retVal(m_bPtrHead, m_vDestructors.size());
  return retVal;
}

mem::stack::~stack(){

  delete[] m_bPtrData;

  m_bPtrData = nullptr;
  m_bPtrHead = nullptr;

}
