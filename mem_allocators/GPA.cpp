void mem::GPA::GPAchunk::get_memory(const size_t &amount){

    m_chunkSize -= amount;
    m_bPtrData = m_bPtrData + amount;

}

void mem::GPA::GPAchunk::moveAndResize(byte* newaddr, const size_t &newSize){

    m_chunkSize = newSize;
    m_bPtrData  = newaddr;

}

template<typename T, typename... arguments>
inline typename std::enable_if<!std::is_trivially_constructible<T>::value>::type
    mem::GPA::constructor(T* object, const size_t &amount, arguments&&... args){
        for (size_t i = 0; i < amount; i++)
            new(std::addressof(object[i])) T(args...);
                
    }

template<typename T>
template<typename T2>
inline typename std::enable_if<std::is_trivially_move_constructible<T2>::value>::type
    mem::GPA::GPApointer<T>::move(void* newaddr){
        std::memmove(newaddr, m_tPtrData, m_size * sizeof(T));
        m_iterator = newaddr + m_iterator - m_tPtrData;
        m_tPtrData = newaddr;
    }

template<typename T>
template<typename T2>
inline typename std::enable_if<!std::is_trivially_move_constructible<T2>::value>::type
    mem::GPA::GPApointer<T>::move(void* newaddr){
        if(newaddr + sizeof(T) < m_tPtrData){

            for (size_t i = 0; i < m_size; i++){
                new(std::addressof(std::static_pointer_cast<T>(newaddr))[i]) T(std::move(m_tPtrData[i]));
                std::addressof(m_tPtrData[i])->~T();
            }

            m_iterator = newaddr + m_iterator - m_tPtrData;
            m_tPtrData = newaddr;

        }else{

            byte tempByteArr[sizeof(T) + alignof(T)];
            T* tempObj = std::static_pointer_cast<T>(nextMultiple<size_t>(alignof(T), (size_t)tempByteArr));

            for (size_t i = 0; i < m_size; i++)
            {
                new(tempObj) T(std::move(m_tPtrData[i]));
                m_tPtrData[i].~T();
                new(std::addressof(std::static_pointer_cast<T>(newaddr)[i])) T(std::move(*tempObj));
                tempObj->~T();
            }

            m_iterator = newaddr + m_iterator - m_tPtrData;
            m_tPtrData = newaddr;       

        }    
        
    }

void mem::GPA::HandleTable::move(void* newaddr){
    movePtr(newaddr);
    m_DataPtrOfGPAPtr = newaddr;
}

void mem::GPA::init(const size_t &size, byte* Location) {

    if(Location == nullptr){

        m_size = size;
        m_bPtrData = new byte[size];
        m_freeChunks.insert(mem::GPA::GPAchunk(m_bPtrData, m_size));
    
    }else{
        
        m_size = size;
        m_bPtrData = Location;
        m_freeChunks.insert(mem::GPA::GPAchunk(m_bPtrData, m_size));

    }
    

}

void mem::GPA::defragment(){

    auto allocated = m_allocatedChunks.begin();
    std::set<mem::GPA::GPAchunk>::iterator mergeunallocated;
    auto unallocated = m_freeChunks.begin();

    byte* tempaddr;
    byte* newaddr;

    while (m_freeChunks.size() > 1)
    {
        if(allocated->m_DataPtrOfGPAPtr < unallocated->m_bPtrData){
            allocated++;
            continue;
        }else{
            tempaddr = (byte*)allocated->m_DataPtrOfGPAPtr;
            newaddr  = (byte*)mem::nextMultiple<size_t>(allocated->align(), (size_t)unallocated->m_bPtrData);
            newaddr[-1] = (byte)((size_t)newaddr - (size_t)unallocated->m_bPtrData);
            const_cast<mem::GPA::GPAchunk*>(&(*unallocated))->moveAndResize(newaddr, unallocated->m_chunkSize - (size_t)newaddr[-1] + tempaddr[-1]);
            const_cast<mem::GPA::HandleTable*>(&(*allocated))->move(newaddr);

            unallocated = m_freeChunks.begin();
            mergeunallocated = std::next(unallocated, 1);

            if(unallocated->m_bPtrData + unallocated->m_chunkSize == mergeunallocated->m_bPtrData){
                const_cast<mem::GPA::GPAchunk*>(&(*unallocated))->m_chunkSize += mergeunallocated->m_chunkSize;
                m_freeChunks.erase(mergeunallocated);
            }
            
            allocated++;
        }

    }
    
    

}

template<typename T, typename... arguments>
bool mem::GPA::getSpace(T* begin, const size_t &ammount, arguments&&... args){

    auto itChunk = m_freeChunks.upper_bound(mem::GPA::GPAchunk((byte*)begin-1, 0));

    if(itChunk->m_bPtrData != (byte*)begin){

        return false;

    }else if(itChunk->m_chunkSize > ammount){

        const_cast<mem::GPA::GPAchunk*>(&(*itChunk))->get_memory(ammount);

        for (size_t i = 0; i < ammount; i++)
            new(std::addressof(begin[i])) T(args...);
        
        return true;

    }else{
        return false;
    }
    


}

template<typename T, typename... arguments>
mem::GPA::GPApointer<T> mem::GPA::allocate(const size_t &size, arguments&&... args){

    static_assert(alignof(T) > 127, "GPA-allocate: max alignment excedet");

    for (auto i = m_freeChunks.begin(); i != m_freeChunks.end(); i++)
    {

        size_t neededSize = size*sizeof(T)+1;
        
        if(i->m_chunkSize > neededSize){

            if(alignof(T) = 0){

                byte* allocationLocation = i->m_bPtrData;
                allocationLocation[0] = 1;
                allocationLocation++;
                const_cast<mem::GPA::GPAchunk>(*i).get_memory(neededSize);
                mem::GPA::GPApointer<T> retVal(size, std::static_pointer_cast<T>(allocationLocation));

                constructor(std::static_pointer_cast<T>(allocationLocation), size, args...);
                
                m_allocatedChunks.insert(mem::GPA::HandleTable::HandleTable(allocationLocation-1, &mem::GPA::GPApointer<T>::move, &mem::GPA::GPApointer<T>::destructor));

                return retVal;

            }

            byte* allocationLocation = (byte*)mem::nextMultiple<T>(alignof(T) + (size_t)i->m_bPtrData);

            size_t offset = (size_t)allocationLocation - (size_t)m_bPtrData;

            if(0 < offset){

                if(i->m_chunkSize > offset + size*sizeof(T)){

                    const_cast<mem::GPA::GPAchunk>(*i).get_memory(offset + size*sizeof(T));
                    allocationLocation[-1] = offset;
                    constructor(std::static_pointer_cast<T>(allocationLocation), size, args...);
                    mem::GPA::GPApointer<T> retVal(size, std::static_pointer_cast<T>(allocationLocation));

                    m_allocatedChunks.insert(mem::GPA::HandleTable(allocationLocation-1, &mem::GPA::GPApointer<T>::move, &mem::GPA::GPApointer<T>::destructor));

                    return retVal;
                }

            }else if(i->m_chunkSize > alignof(T) + size*sizeof(T)) {
                const_cast<mem::GPA::GPAchunk>(*i).get_memory(alignof(T) + size*sizeof(T));
                allocationLocation += alignof(T);
                allocationLocation[-1] = alignof(T);
                constructor(std::static_pointer_cast<T>(allocationLocation), size, args...);
                mem::GPA::GPApointer<T> retVal(size, std::static_pointer_cast<T>(allocationLocation));

                m_allocatedChunks.insert(mem::GPA::HandleTable(allocationLocation-1, &mem::GPA::GPApointer<T>::move, &mem::GPA::GPApointer<T>::destructor));

                return retVal;
            }
            

        }

    }

    return nullptr;

}

template<typename T>
void mem::GPA::deallocate(mem::GPA::GPApointer<T> pointer) {

    static_assert(alignof(T) > 127, "GPA-dealocate:  max alignment excedet");

    size_t offset    = std::static_pointer_cast<byte>(pointer.m_tPtrData)[-1];
    size_t chunkSize = offset + (pointer.m_chunkSize * sizeof(T));
    byte* memStartLocation = std::static_pointer_cast<byte>(pointer.m_tPtrData) - offset;

    std::pair<std::set<mem::GPA::GPAchunk>::iterator, bool> chunkPosition = m_freeChunks.insert(mem::GPA::GPAchunk(memStartLocation, chunkSize));
    std::set<mem::GPA::GPAchunk>::iterator mergeChunk = std::next(chunkPosition.first, 1);

    if(chunkPosition.first->m_bPtrData + chunkPosition.first->m_chunkSize == mergeChunk->m_bPtrData){

        const_cast<mem::GPA::GPAchunk>(*chunkPosition.first).m_chunkSize = const_cast<mem::GPA::GPAchunk>(*chunkPosition.first).m_chunkSize + mergeChunk->m_chunkSize;
        m_freeChunks.erase(mergeChunk);

    }

    mergeChunk = std::next(chunkPosition.first, -1);

    if(chunkPosition.first->m_bPtrData == mergeChunk->m_bPtrData + mergeChunk->m_chunkSize){

        const_cast<mem::GPA::GPAchunk>(*mergeChunk).m_chunkSize = const_cast<mem::GPA::GPAchunk>(*mergeChunk).m_chunkSize + chunkPosition.first->m_chunkSize;
        m_freeChunks.erase(chunkPosition.first);

    }

    for (size_t i = 0; i < pointer.m_chunkSize; i++)
        (pointer + i)->~T();

}
