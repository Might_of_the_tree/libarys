//only enable when you compile without memoryAllocators.hpp
//#include "includes.hpp"


namespace mem{

  class stackDestructor{
  private:
    const void* m_vPtrData;
    void(*destructor)(const void*);

  public:
      template<typename T>
      stackDestructor(const T &data);
      void operator()();

  };

  struct stackMarker{

    byte* m_bPtrData;
    size_t m_destructorC;

    stackMarker(byte* bPtrData, const size_t &destructorC)
    :m_bPtrData(bPtrData), m_destructorC(destructorC){}

  };

  class stack{
  private:

    size_t    m_size;
    byte* m_bPtrHead;
    byte* m_bPtrData;

    std::vector<stackDestructor> m_vDestructors;

    template<typename T>
    inline typename std::enable_if<std::is_trivially_destructible<T>::value>::type
      addDestructorToList(T* object){
        //do nothing
      }

    template<typename T>
    inline typename std::enable_if<!std::is_trivially_destructible<T>::value>::type
      addDestructorToList(T* object){
        m_vDestructors.push_back(stackDestructor(*object));
      }

  public:

    void init(const size_t &size, byte* Location);
    void* allocate(const size_t &bytesC, const size_t &allignment);
    template <typename T, typename... arguments>
    T* allocateObject(size_t ObjectsC, arguments&&... args);
    template<typename T, typename... arguments>
    bool getSpace(T* begin, const size_t &ammount, arguments&&... args);
    void deallocateALL();
    void deallocate(const mem::stackMarker &marker);//will erase the data from m_tPtrHead to data
    mem::stackMarker get_marker();

    stack(const stack &           ) = delete;
    stack(stack &&                ) = delete;
    stack& operator=(const stack &) = delete;
    stack& operator=(stack &&     ) = delete;

    stack(){}
    ~stack();

  };

}
