#include "../src/vector.cpp"
#include <iostream>


int main(int argc, char const *argv[]) {

  D_vector<int> Vt1(100);
  D_vector<int> Vt2(200);
  D_vector<int> Vt3(100);

  for (size_t i = 0; i < 100; i++)
    Vt1[i]=i;

  Vt2=Vt1;
  Vt3=Vt1+Vt2;

  if(Vt3.get_vectorValue() != Vt2.get_vectorValue()+Vt1.get_vectorValue())
    return -10;

  std::cout << "succes";

  return 0;
}
