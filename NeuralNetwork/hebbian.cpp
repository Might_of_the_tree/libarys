#include "hebbian.hpp"

double& linear(double &x) {

  return x;

}

NN::hebbian::bias::bias(const double &ftdOutput, std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput)
 : m_ftdOutput(ftdOutput) {

  for (auto i = VftdPtrConnectionsOutput.begin(); i != VftdPtrConnectionsOutput.end(); i++)
     m_Vconnection.push_back(NN::hebbian::connection(*i));

}

inline void NN::hebbian::bias::update_output(){

  for (auto i = m_Vconnection.begin(); i != m_Vconnection.end(); i++)
    i->update_output(m_ftdOutput);
  
}

inline void NN::hebbian::bias::update_connections(){

  for (auto i = m_Vconnection.begin(); i != m_Vconnection.end(); i++)
    i->learn(m_ftdOutput);

}

NN::hebbian::input::input(double* ftdPtrInput, double& (*akt)(double&), std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput)
 : m_ftdPtrInput(ftdPtrInput), m_akt(akt) {

  for (auto i = VftdPtrConnectionsOutput.begin(); i != VftdPtrConnectionsOutput.end(); i++)
     m_Vconnection.push_back(NN::hebbian::connection(*i));

}

inline void NN::hebbian::input::update_output(){

  for (auto i = m_Vconnection.begin(); i != m_Vconnection.end(); i++)
    i->update_output(*m_ftdPtrInput);
  
}

inline void NN::hebbian::input::update_connections(){

  for (auto i = m_Vconnection.begin(); i != m_Vconnection.end(); i++)
    i->learn(*m_ftdPtrInput);
  
}

NN::hebbian::neuron::neuron(std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput, std::vector<double>* VPtrftdInputs, double& (*akt)(double&))
 : m_VPtrftdInput(VPtrftdInputs), m_akt(akt) {

   for (auto i = VftdPtrConnectionsOutput.begin(); i != VftdPtrConnectionsOutput.end(); i++)
     m_Vconnection.push_back(NN::hebbian::connection(*i));

}

inline void NN::hebbian::neuron::init_connections(std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput) {

  for (auto i = VftdPtrConnectionsOutput.begin(); i != VftdPtrConnectionsOutput.end(); i++)
     m_Vconnection.push_back(NN::hebbian::connection(*i));

}

inline void NN::hebbian::neuron::update_output() {

  m_ftdOutput = 0;

  for (auto i = m_VPtrftdInput->begin(); i != m_VPtrftdInput->end(); i++)
    m_ftdOutput += (*i);

  m_ftdOutput = m_akt(m_ftdOutput);

  for (auto i = m_Vconnection.begin(); i < m_Vconnection.end(); i++)
    i->update_output(m_ftdOutput);
  
}

inline void NN::hebbian::neuron::update_connections() {

  for (auto i = m_Vconnection.begin(); i < m_Vconnection.end(); i++)
    i->learn(m_ftdOutput);
  
}

NN::hebbian::hebbian(std::vector<int> &ViNeuronC, std::vector<double*> &Vinput, std::vector<double> &VftdBias, std::vector<std::vector<double& (*)(double&)>> &aktivation, std::vector<int*> &connections){

  static std::vector<std::vector<std::vector<double>>> V3Dinputs;
  std::vector<std::vector<std::vector<NN::hebbian::connection::output>>> V3Doutputs;
  V3Dinputs .assign(ViNeuronC.size(), std::vector<std::vector<double>>());

  //check if the connections are defined
  if(connections.size() == 0){
    //output pointer for the connections
    V3Doutputs.assign(ViNeuronC.size()+1, std::vector<std::vector<NN::hebbian::connection::output>>());
    //variables for efficient loops
    auto i1 = ViNeuronC .begin();
    auto i2 = V3Dinputs .begin();
    auto i3 = V3Doutputs.begin();
    size_t i5 = 0;
    size_t i6 = 0;
    
    //input vector of neurons
    i2->assign(*i1, std::vector<double>());

    //create input vectors of first neuron layer
    for (auto i = i2->begin(); i != i2->end(); i++)
      i->assign(VftdBias.size()+Vinput.size(), double());

    //create input vectors of the other neuron layers
    for (i1; i1 != ViNeuronC.end(); i1++) {
      i2++;
      i2->assign(*i1, std::vector<double>());

      for(auto i = i2->begin(); i != i2->end(); i++)
        i->assign(i1[-1], double());

    }
    //reseting iterator
    i1 = ViNeuronC.begin();
    i2 = V3Dinputs.begin();
    //allocating connection output vector for Bias
    i3->assign(VftdBias.size(), std::vector<NN::hebbian::connection::output>());
    //assigning connection output pointer
    for (auto i = i3->begin(); i != i3->end(); i++){
      i->assign((*i1), std::vector<NN::hebbian::connection::output>);
      i5++;

      for(size_t i4 = 0; i4 < (*i1))
        (*i)[i4].push_back(NN::hebbian::connection::output(&(*i2)[i4][i5]));

    }
    //next neuron layer(input neurons)
    i3++;
    i3->assign(Vinput.size(), std::vector<NN::hebbian::connection::output>());

    for (auto i = i3->begin(); i != i3->end(); i++){
      i->assign((*i1), std::vector<NN::hebbian::connection::output>);
      i5++;//i5 shouldn't be 0 before this loop

      for(size_t i4 = 0; i4 < (*i1))
        (*i)[i4].push_back(NN::hebbian::connection::output(&(*i2)[i4][i5]));

    }
    //next layer
    i3++;
    //assigning connection output pointer for the other connections
    for(i3; i3 != V3Doutputs.end(); i3++){
      i3->assign(i1[i6], std::vector<NN::hebbian::connection::output>());
      i6++;
      i5 = 0;

      for (auto i = i3->begin(); i != i3->end(); i++){
        i->assign(i1[i6], std::vector<NN::hebbian::connection::output>());
        i5++;

        for(size_t i4 = 0; i4 < (*i1))
          (i[i6])[i4].push_back(NN::hebbian::connection::output(&i2[i6][i4][i5]));

      }

    }
    
  }else{
    V3Doutputs.assign(ViNeuronC.size()+2, std::vector<std::vector<NN::hebbian::connection::output>>());
    //TODO create input vectors with connection information
  }
  
  //TODO create every neuron and assign their connections

}
