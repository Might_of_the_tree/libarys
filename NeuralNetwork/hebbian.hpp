#include <vector>

#ifndef standardWeight
#define standardWeight 1
#endif

typedef unsigned long int size_t;

namespace NN{

  class hebbian{
  public:

    struct constuct_data{

      std::vector<int> m_ViNeuronC;//neuron count in each layer
      std::vector<std::vector<int*>> m_ViPtrConnectionC;//connection count in each layer
      std::vector<double> m_VftdBias;//bias
      std::vector<double*>     m_Vinput;
      std::vector<std::vector<void*>> m_VvPtraktivation;//the aktivation funktion for each neuron

    };

  private:

    class connection{
    private:
      double m_ftdWeight;//the current weight of the connection
      NN::hebbian::connection::output m_output;
    public:
      connection(NN::hebbian::connection::output thisoutput): m_output(thisoutput), m_ftdWeight(standartWeight){}
      ~connection(){}

      struct output
      {
        double* m_ftdPtrOutput;
        double* m_ftdPtrEnd_output;
        output(double* ftdPtrOutput) : m_ftdPtrOutput(ftdPtrOutput) {}
      };

      inline void learn(const double &ftdInput){ m_ftdWeight = ftdInput * (*m_output.m_ftdPtrEnd_output); }
      inline void update_output(const double &ftdInput){ (*m_output.m_ftdPtrOutput) = ftdInput * m_ftdWeight; }
    };

    class bias{
    private:
      double m_ftdOutput;//the output

      std::vector<NN::hebbian::connection> m_Vconnection;//connections
    public:

      bias(const double &ftdOutput, std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput);
      ~bias(){}

      inline void update_output();
      inline void update_connections();
    };

    class input{
    private:
      double* m_ftdPtrInput;//input of the neuron
      std::vector<NN::hebbian::connection> m_Vconnection;//connections
      double& (*m_akt)(double&);//aktivation function
    public:

      input(double* ftdPtrInput, double& (*akt)(double&), std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput);
      ~input(){}

      inline void update_output();//funktion to get the output
      inline void update_connections();//funktion to learn
    };

    class neuron{
    private:
      std::vector<double>* m_VPtrftdInput;// every input from connections
      std::vector<NN::hebbian::connection> m_Vconnection;//the connections starting at this neuron
      double& (*m_akt)(double&);//pointer to the aktivation funktion
      double m_ftdOutput;
    public:

      neuron(std::vector<double>* VPtrftdInputs, double& (*akt)(double&)) : m_VPtrftdInput(VPtrftdInputs), m_akt(akt){}
      neuron(std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput, std::vector<double>* VPtrftdInputs, double& (*akt)(double&));
      ~neuron(){}

      inline void init_connections(std::vector<NN::hebbian::connection::output> &VftdPtrConnectionsOutput);
      inline void update_output();
      inline void update_connections();
    };

      std::vector<NN::hebbian::bias> m_Vbias;
      std::vector<NN::hebbian::input> m_Vinput;
      std::vector<std::vector<NN::hebbian::neuron>> m_V2Dneurons;

      NN::hebbian::constuct_data this_NN;//the data to create exactly the same neural_network

  public:

      hebbian(std::vector<int> &ViNeuronC, std::vector<double*> &Vinput, std::vector<double> &VftdBias, std::vector<std::vector<double& (*)(double&)>> &aktivation, std::vector<int*> &connections);
      ~hebbian();

      std::vector<double>& run();//run the neural_network and get the outputs back
      void learn();//change the weights
      inline NN::hebbian::constuct_data& get_construct(){ return this_NN; }//get the data for construction

  };

}

#undef standardWeight
